<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- InstanceBeginEditable name="doctitle" -->
  <title>Campus Technology | Western Washington University</title>
  <!-- InstanceEndEditable -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="wwu_shila/build/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="wwu_shila/build/css/components.css">
  <link rel="stylesheet" type="text/css" href="includes/css/main.css">
  <link rel="stylesheet" type="text/css" href="customize/customize.css">
  <!-- InstanceBeginEditable name="head" -->
  <!-- InstanceEndEditable -->
  <!--#include virtual="customize/analytics.html"-->
</head>

<body>
  <!--#include virtual="includes/header.shtml"-->
  <main>
    <!-- InstanceBeginEditable name="Main Content" -->
    <section class="intro">
      <div class="panel">
        <h1>Western Technologies</h1>
        <p>
          Western is proud to host modern technologies supporting both academic and co-curricular activities. Accessible computer labs, laptop checkouts, cloud-based storage and communication tools are all in common use. Advanced scientific instrumentation, prototyping and design systems, as well as creative tools for media production are available in many courses.
        </p>
      </div>
    </section>

    <section class="features">
      <div class="panel feature">
        <div class="info-container dark-bg">
          <h2 class="department-name">Media Production</h2>
          <p>
            Students may borrow a variety of field equipment including professional-grade cameras, lenses and camera accessories such as a gimbal stabilizer. The Digital Media Center is a multi-camera TV studio with a green screen and cyclorama wall available to both classes and individuals students.
          </p>
        </div>
        <div class="image-container">
          <img class="feature-image" src="images/media_production.jpg" alt="a set of high end video cameras and lights point towards a green screen"></img>
        </div>
        <span aria-hidden="true" class="decorative border"></span>
        <span aria-hidden="true" class="decorative block-one"></span>
        <span aria-hidden="true" class="decorative block-two"></span>
      </div>

      <div class="panel feature">
        <div class="info-container dark-bg">
          <h2 class="department-name">New Technologies</h2>
          <p>
            The University’s Student Technology Fee is used to keep labs, laptops and other student equipment up-to-date. This fund is also used to provide leading-edge tools for student instruction and research such as an Augmented Reality sandbox in the Geology program.
          </p>
        </div>
        <div class="image-container">
          <img class="feature-image" src="images/placeholder.jpg" alt="you must provide descriptive text for all images"></img>
        </div>
        <span aria-hidden="true" class="decorative border"></span>
        <span aria-hidden="true" class="decorative block-one"></span>
        <span aria-hidden="true" class="decorative block-two"></span>
      </div>

      <div class="panel feature">
        <div class="info-container dark-bg">
          <h2 class="department-name">Collaborate with Peers and Faculty</h2>
          <p>
            Resources such as Office 365, Google Docs and Canvas provide opportunities for online collaboration while collaborative smartboards are available for teamwork in the Library.
          </p>
        </div>
        <div class="image-container">
          <img class="feature-image" src="images/collaboration_with_peers.jpg" alt="A hand holding a stylus interacting with a screen. The pen touches an onscreen 'processing' button"></img>
        </div>
        <span aria-hidden="true" class="decorative border"></span>
        <span aria-hidden="true" class="decorative block-one"></span>
        <span aria-hidden="true" class="decorative block-two"></span>
      </div>

      <div class="panel feature">
        <div class="info-container dark-bg">
          <h2 class="department-name">Build an Online Portfolio</h2>
          <p>
            Through an emerging scientific process called gene drives, scientists could alter the genetics of mosquitos to prevent them from passing along these diseases to the human population. Someday, scientists could use the technology we have now to alter humanity to make us all more cancer resistant, for example. But the gene drives—and the power they create—are so controversial and riddled with moral and ethical scientific dilemma that the world has collectively slow-tracked their use. An ideal candidate for the use gene drives...
            <a href="http://window.wwu.edu/article/112165" target="_blank">Read the whole story in Window</a>
          </p>
        </div>
        <div class="image-container">
          <img class="feature-image" src="images/online_portfolio.png" alt="A sample portfolio site, with the words 'React, Research, Execute!' at the top. Portfolio showcases public art pieces with related articles."></img>
        </div>
        <span aria-hidden="true" class="decorative border"></span>
        <span aria-hidden="true" class="decorative block-one"></span>
        <span aria-hidden="true" class="decorative block-two"></span>
      </div>

      <div class="panel feature">
        <div class="info-container dark-bg">
          <h2 class="department-name">Student Technology Center</h2>
          <p>
            Our Student Technology Center assists all students with software skill development and provides scanning, large-format printing, as well as 3D printing services. New services include a Virtual Reality Lab and workshops in robotics and home automation.
          </p>
        </div>
        <div class="image-container">
          <img class="feature-image" src="images/placeholder.jpg" alt="you must provide descriptive text for all images"></img>
        </div>
        <span aria-hidden="true" class="decorative border"></span>
        <span aria-hidden="true" class="decorative block-one"></span>
        <span aria-hidden="true" class="decorative block-two"></span>
      </div>

      <div class="panel feature">
        <div class="info-container dark-bg">
          <h2 class="department-name">Technology within Programs</h2>
          <p>
            Many programs at Western provide direct hands-on experience with leading technologies. The Industrial Technology-Vehicle Design program engages students in prototyping vehicles in the automotive, aerospace, and marine industries. Students in the sciences may perform analyses with equipment such as scanning electron microscopes or mass spectrometers. Our Computer Science department hosts many active clubs in robotics, AI and game design.
          </p>
        </div>
        <div class="image-container">
          <img class="feature-image" src="images/placeholder.jpg" alt="you must provide descriptive text for all images"></img>
        </div>
        <span aria-hidden="true" class="decorative border"></span>
        <span aria-hidden="true" class="decorative block-one"></span>
        <span aria-hidden="true" class="decorative block-two"></span>
      </div>

    </section>

    <!-- InstanceEndEditable -->
  </main>

  <!--#include virtual="includes/footer.shtml"-->
  <!--#include virtual="includes/scripts.html"-->

</body>
</html>
