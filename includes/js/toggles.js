/* Button Toggles */
var searchButton = $(".toggle-search");
var searchWidget = $(".western-search-widget");

// Search
searchWidget.hide();

searchButton.click(function () {
	searchWidget.animate({width: 'toggle'});
});

// Western Quick Links
var quickLinksButton = $(".toggle-quick-links");
var quickLinksWidget = $(".western-quick-links");

quickLinksButton.click(function () {
	quickLinksWidget.animate({width: 'toggle'});
});

// Mobile Main Navigation
var mainNavButton = $(".toggle-main-nav");
var mainNav = $(".nav--main");

mainNavButton.click(function () {
	mainNav.slideToggle();
});

/**
* Click event handler for wester-search-widget.  If checkbox is checked then
* set search profile (pr) to 'Default-WWU'.
* @function checkSearchWestern
* @private
*/
function setSearchProfile() {
	if (document.getElementById("western-search-western").checked == true) {
		document.getElementsByName('pr')[0].value = 'Default-WWU';
	}
}
