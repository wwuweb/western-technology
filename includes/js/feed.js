//This is the feed for the Research RSS.
//https://westerntoday.wwu.edu/taxonomy/term/4753/all/feed

//This is the Faculty Research feed.
//https://westerntoday.wwu.edu/taxonomy/term/4395/all/feed

loadRss("https://westerntoday.wwu.edu/taxonomy/term/4753/all/feed", document.getElementById("research"), "<h2>Research in the News</h2>", 3);
loadRss("https://westerntoday.wwu.edu/taxonomy/term/4395/all/feed", document.getElementById("facresearch"), "<h2>Faculty Research</h2>", 3);

function loadRss(rssurl, location, header, listLength)
{
    location.innerHTML = header;
    $.get(rssurl, function(data) {
    var $xml = $(data);
    var i=1;
    $xml.find("item").each(function() {
        var $this = $(this),
            item = {
                title: $this.find("title").text(),
                link: $this.find("link").text(),
                //description: $this.find("description").text(),
                //pubDate: $this.find("pubDate").text(),
                //author: $this.find("author").text()
            }

            if (i <= listLength)
                    location.innerHTML +=
                    "<ul>" +
                      "<li><a href='" + item.link + "'>" + item.title + "</a></li>" +
                    "</li>";
            i++;

            });
        });
}
