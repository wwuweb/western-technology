require 'breakpoint'
require 'sass-globbing'

sass_dir        = "includes/sass"
css_dir         = "includes/css"
images_dir      = "images"
javascripts_dir = "js"

relative_assets = true
