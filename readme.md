# Western Technology Page
This is the static "template" for the technology site, originally developed for the research site.

It has some nifty features including a rebuilt header and footer using CSS Grids, and styles tied to the [living style guide](https://bitbucket.org/wwuweb/wwu-shila) that is currently under development.

The altern-aligned blue blocks/body section is all styled within a customize.css file, as I don't envision that being a consistent design across every potential use of this template.

Because [wwu-shila](https://bitbucket.org/wwuweb/wwu-shila)/the living style guide are still under development, there are a lot of patterns that are included in this repo that will need to be revisited in the future, including the aforementioned header and footer. As the guide is developed further, much of the SASS in this repo will likely need to be replaced or removed so that it can stay current with the progress of the style guide.

My hope is that it can serve as a future single page and/or static site template that is consistent with future Drupal sites using the style guide as a base theme. It will for sure need some tweaking if this is the case.

**Important**: Because of it's ties to the style guide, this site does currently require two compiled css files from [wwu-shila](https://bitbucket.org/wwuweb/wwu-shila) in order to display properly. In the long run I envision those compiled css files living somewhere like wwucommon, but in the meantime wwu-shila just needs to exist and be compiled within this repo on the site.

To install:

0. Clone this repository: `git clone git@bitbucket.org:wwuweb/western-technology.git`
0. cd into western-technology and clone wwu-shila: `git clone git@bitbucket.org:wwuweb/wwu-shila.git`
0. Run `bundle exec compass compile` from the base directory (this needs to be done *after* wwu-shila is cloned or it will throw sass errors)
0. cd into wwu-shila and run `npm install gulp` then `gulp sass`

The bundle command can be run from vagrant, but [node](https://nodejs.org/en/) needs to have a newer version installed than what's in our vagrant (I have 9.10.1 installed as of testing, but anything >8.0.0 is probably also fine) and [gulp](https://gulpjs.com/) needs to be installed too so the last step is easier to do locally.
